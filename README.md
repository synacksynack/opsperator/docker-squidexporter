# k8s Prometheus Squid Exporter

Based on https://github.com/boynux/squid-exporter

Build with:

```
$ make build
```

Start Demo in OpenShift:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Setting up Squid, see
https://dev.mysql.com/doc/refman/5.7/en/innodb-information-schema-metrics-table.html

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name           |   Description      | Default     |
| :------------------------- | ------------------ | ----------- |
|  `SQUID_EXPORTER_LISTEN`   | Exporter Address   | `:9113`     |
|  `SQUID_PORT`              | Squid Address      | `8080`      |
