FROM docker.io/alpine/git AS cloner

# Squid Exporter image for OpenShift Origin

ENV SQUIDEXPORTER_ROOTURL=https://github.com/boynux/squid-exporter/ \
    SQUIDEXPORTER_VERSION=v1.9.4

RUN set -x \
    && mkdir -p /usr/src \
    && cd /usr/src \
    && git clone -b $SQUIDEXPORTER_VERSION $SQUIDEXPORTER_ROOTURL

FROM docker.io/golang:alpine AS builder

COPY --from=cloner /usr/src/squid-exporter /go/src/github.com/boynux/squid-exporter

RUN set -x \
    && if test `uname -m` = aarch64; then \
	export GOARCH=arm64; \
    elif test `uname -m` = armv7l; then \
	export GOARCH=arm; \
    else \
	export GOARCH=amd64; \
    fi \
    && cd /go/src/github.com/boynux/squid-exporter \
    && CGO_ENABLED=0 \
	GOOS=linux \
	go install -a -ldflags '-extldflags "-s -w -static"' .

FROM scratch

LABEL io.k8s.description="Squid Prometheus Exporter Image." \
      io.k8s.display-name="Squid Prometheus Exporter" \
      io.openshift.expose-services="9301:http" \
      io.openshift.tags="prometheus,exporter,squid" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-squidexporter" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="1.9.4"

COPY --from=builder /go/bin/squid-exporter /usr/local/bin/squid-exporter
COPY --from=builder /etc/nsswitch.conf /etc/nsswitch.conf

ENTRYPOINT ["/usr/local/bin/squid-exporter"]
USER 1001
